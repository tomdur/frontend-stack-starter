module.exports = config = {
    mode: 'development' //development or production
}

// LOCAL DEV:
// 1. /config.js: change to development
// 2. /src/es6/config.js: change current to devLocal
// 3. /src/scss/_variables.scss: change $pathToAssets to /assets/

// WP DEV:
// 1. /config.js: change to production
// 2. /src/es6/config.js: change current to devBackend
// 3. /src/scss/_variables.scss: change $pathToAssets to /spot/wp-content/themes/spot_poznan/assets/
// 4. build

// PRODUCTION:
// 1. /config.js: change to production
// 2. /src/es6/config.js: change current to prod
// 3. /src/scss/_variables.scss: change $pathToAssets to /wp-content/themes/pohid/assets/
// 4. build