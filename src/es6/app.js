require('../scss/main.scss');

// ie11: custom added poly as addition to babel-polyfill (webpack.config)
import './polyfills';

class mainApp {

    constructor() {

    }

    initApp() {
        document.addEventListener("DOMContentLoaded", e=>{

            console.log('JS app bootstrap. Edit me in: /src/es6/app.js');


            // inits after dom loaded
            this.afterDomLoaded();

            // fade in body
            this.fadePageContent();
        });

        window.addEventListener('load', e=>{
            // inits after assets loaded

        })
    }

    afterDomLoaded() {
        // init classes like:
        // new HttpRequest();


    }

    fadePageContent() {
        document.querySelector('body').style.opacity = 1;
    }


}

const app = new mainApp;
app.initApp();
// window.app = app; // app added to window scope
