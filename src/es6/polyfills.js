import 'url-search-params-polyfill'; // ie11: URLSearchParams

//native elem.scroll() override (ie / edge)
import smoothscroll from 'smoothscroll-polyfill'; smoothscroll.polyfill();

if (!Element.prototype.matches) { // ie11: closest() 
    Element.prototype.matches = Element.prototype.msMatchesSelector || 
                                Element.prototype.webkitMatchesSelector;
  }
  
  if (!Element.prototype.closest) {
    Element.prototype.closest = function(s) {
      var el = this;
  
      do {
        if (el.matches(s)) return el;
        el = el.parentElement || el.parentNode;
      } while (el !== null && el.nodeType === 1);
      return null;
    };
  }


if (!NodeList.prototype.forEach) // ie11: forEach for node list
{
    NodeList.prototype.forEach = function(fun)
    {
        var len = this.length;
        if (typeof fun != "function")
            throw new TypeError();

        var thisp = arguments[1];
        for (var i = 0; i < len; i++)
        {
            if (i in this)
                fun.call(thisp, this[i], i, this);
        }
  };
}
