import config from './config';

export class HttpRequest {

    constructor() {
        this.http = new XMLHttpRequest();
    }

    // post request
    post(data) {

        const storeHttp = this.http;

        const formData = new URLSearchParams();
        formData.append('action', data.action); // wordpress specific
        formData.append('payload', data.payload);

        return new Promise((resolve, reject)=>{

            this.http.open('POST', config[config.current].ajaxApi, true);
            this.http.setRequestHeader('content-type', 'application/x-www-form-urlencoded; charset=UTF-8');
            this.http.send(formData);
            this.http.onload = ()=>{
        
                if (storeHttp.status === 200) {
                    resolve(JSON.parse(storeHttp.responseText));
        
                } else {
                    reject(`Error: ${storeHttp.status}`);

                }
            }
            this.http.onerror = (err) => {
                reject(`Error: ${err}`);
            }
        });
    }

}