export default {
    current: 'devLocal', // set current env
    devLocal: { // frontend stack only
        ajaxApi: 'http://evoltec.local/wp-admin/admin-ajax.php', // local development backend url for ajax calls from HttpRequest
        pathToAssets: '/assets/',   // local front dev assets (src/assets/) whenever needed within js
    },
    devBackend: { // build for local backend devel
        ajaxApi: 'http://evoltec.local/wp-admin/admin-ajax.php',
        pathToAssets: '/wp-content/themes/evoltec/assets/',
    },
    prod: { // prod deployment
        ajaxApi: '/wp-admin/admin-ajax.php',
        pathToAssets: '/wp-content/themes/evoltec/assets/',
    }
}