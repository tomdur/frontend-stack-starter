require("@babel/polyfill"); // ie11
const config = require('./config'); // custom config file
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin'); // html: combine from partials (after twig-loader)
const UglifyJsPlugin = require('uglifyjs-webpack-plugin'); // js: uglify
const MiniCssExtractPlugin = require("mini-css-extract-plugin"); // extract css to external file in production
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin'); // css: strip comments and uglify
const CopyWebpackPlugin = require('copy-webpack-plugin'); // copy static assets to dist
// const workboxPlugin = require('workbox-webpack-plugin'); // service worker by workbox

module.exports = {
    mode: config.mode,
    entry: [
        "@babel/polyfill", // ie11
        './src/es6/app.js'
    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        // filename: 'app.[hash].js',
        filename: 'app.js',
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        host: '127.0.0.1',
        port: 9000
    },
    module: {
        rules: [
            {   test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            },
            {
                test: /\.twig$/,
                loader: "twig-loader",
                options: {
                },
            },
            {
                test: /\.scss$/,
                use: [
                    //prod: extract to file, dev: keep in main .js
                    config.mode === 'development' ? 'style-loader' : MiniCssExtractPlugin.loader,
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.(eot|woff|woff2|ttf)$/i,
                loader: "file-loader",
                options: {
                    outputPath: 'fonts',
                },
            },
            {
                test: /\.(png|jpg|gif|svg)$/i,
                loader: "file-loader",
                options: {
                    outputPath: 'img',
                },
                // use: [
                //   {
                //     loader: 'url-loader',
                //     options: {
                //       limit: 8000
                //     } 
                //   }
                // ]
              }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/twig/index.twig',
            favicon: './src/assets/img/favicon.png',
            pathToAssets: config.mode === 'development' ? '/assets/' : '/assets/',
        }),
        new MiniCssExtractPlugin({
            // add hash to name in production
            // filename: config.mode === 'development' ? "[name].css" : "[name].[hash].css",
            // chunkFilename: config.mode === 'development' ? "[id].css" : "[id].[hash].css"
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new CopyWebpackPlugin([
            {from:'src/assets',to:'assets'} 
        ]),
        // new workboxPlugin.GenerateSW({ // this will override custom sw.js
        //     swDest: 'sw.js',
        //     clientsClaim: true,
        //     skipWaiting: true,
        //   })
      ],
    optimization: {
        splitChunks: {
            chunks: 'all'
        },
        minimizer: [new UglifyJsPlugin({
            sourceMap: false,
            extractComments: false,
            uglifyOptions: {
                compress: {
                    sequences: true,
                    dead_code: true,
                    conditionals: true,
                    booleans: true,
                    unused: true,
                    if_return: true,
                    join_vars: true,
                    drop_console: true
                },
                mangle: true,
                output: {
                    comments: false
                }
            }
        }),
        new OptimizeCssAssetsPlugin({})
        ]
    },
    // resolve: {
    //     alias: {
    //         "TweenLite": path.resolve('node_modules', 'gsap/src/uncompressed/TweenLite.js'),
    //         "TweenMax": path.resolve('node_modules', 'gsap/src/uncompressed/TweenMax.js'),
    //         "TimelineLite": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineLite.js'),
    //         "TimelineMax": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineMax.js'),
    //         "ScrollMagic": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js'),
    //         "animation.gsap": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js'),
    //         "debug.addIndicators": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js'),
    //         "TweenLiteMin": path.resolve('node_modules', 'gsap/src/minified/TweenLite.min.js'),
    //         "TimelineLiteMin": path.resolve('node_modules', 'gsap/src/minified/TimelineLite.min.js'),
    //     }
    // }
};